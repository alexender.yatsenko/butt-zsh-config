### [thornintheass](https://gitlab.com/alexender.yatsenko) zsh config

## Installation


```sh
git clone --recurse-submodules https://gitlab.com/vapenyk/butt-zsh-config.git ~/butt-zsh-config
```

```zsh
# add source to ~/.zshrc
echo 'source /home/$USER/butt-zsh-config/butt-zshrc.zsh' >> ~/.zshrc
```
